const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'main-[hash].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    // Extract CSS into a separate file
                    { loader: MiniCssExtractPlugin.loader },            
                    // Translates CSS into CommonJS
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    // transforming CSS to Use tomorrow’s CSS today!
                    'postcss-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
            {
                test: /\.?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(png|jpe?g|gif|ttf)$/i,
                use: [
                    {
                      loader: 'file-loader',
                    },
                ],
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: '[name]-[hash]-bundle.css',
        }),
        new HtmlWebpackPlugin(),
    ],
    devtool: 'cheap-module-source-map',
    mode: 'production',
}
